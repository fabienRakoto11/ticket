[![-----------------------](https://raw.githubusercontent.com/alvin-ictn/readme/master/assets/images/lines/colored.png)](#-description)

## ➤ Description
This is the repository  for ticket project 


[![-----------------------](https://raw.githubusercontent.com/alvin-ictn/readme/master/assets/images/lines/colored.png)](#-user-story)

## ➤ project install
1. Clone or fork the project first
2. install composer dependancy : executing ``` composer install ```
3. install the node js depandancy  executing ``` npm install ``` 
4. update your .env file
5. generate a new application key : ``` php artisan key:generate ```
6. Run the migration  : ``` php artisan migrate ```
7.  That it :) 

[![-----------------------](https://raw.githubusercontent.com/alvin-ictn/readme/master/assets/images/lines/colored.png)](#-hint)


Happy coding 🥚
